import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Provider } from 'react-redux';
import { Image, StyleSheet, Text, View } from 'react-native';

import store from './app/store';
import Home from './app/components/Home.jsx';

export default function App() {
  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <Image
        style={styles.logo}
        source={require('./assets/farm.png')}
      />
      <Text style={styles.title}>HelloFarm, your trusty Farm Manager</Text>
      <Provider store={store}>
        <Home/>
      </Provider>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingVertical: 50,
    paddingHorizontal: 20,
  },
  logo: {
    width: 100,
    height: 100,
  },
  title: {
    paddingTop: 30,
    fontWeight: 'bold',
    fontSize: 25,
    textAlign: 'center',
  }
});
