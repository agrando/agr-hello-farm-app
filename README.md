# hellofarm-app

This is a small project for recruitment interview purposes at Agrando. It's created with expo-cli and includes redux and axios.
You would need to have a working emulator installed and if possible make sure that the project runs well before the interview.

## Setup & Development

##### Setup expo globally
```
 yarn global add expo-cli
```


##### Setup node modules
```
yarn install
```

##### Compiles and hot-reloads for development
```
expo start
```

## Assignment
The overall aim is to create a mobile-app to keep track of how many animals we have on our farm. This project includes React native and Redux. You're free to use them as you see fit and also install any further packages. In the end it could look similar to this:

<!-- ![hello-Farm-Example.png](./assets/example-hello-farm-app.png) -->
<img src="./assets/example-hello-farm-app.png" width="350" alt="hello-Farm-Example.png">

The following tasks should be completed in order. The aim of this assignment is not to complete as many tasks as possible, but to write sensible code, create a working application and gain a better understanding of what it would be like to work together.

1. **Create a main screen with counters for at least 3 species of animals, pigs, cows and sheep.**

 Each counter starts out with 0 and consists of a button to increase the amount, a text showcasing the current amount and a button to decrease the amount. You should not be able to decrease the amount below 0.
 
 
2. **Save the state of your farm manager on firebase.** 

Add a submit button to your screen. When the user presses the button the app should send a PUT request with the state of your counters to the endpoint `https://hello-farm-ceb1d.firebaseio.com/farms/<your-farm-name>.json`. You can choose the name of your farm yourself. While the request happens the user should see some kind of indicator that the app is loading.


3. **Load the current state of your farm from the backend.**

Your app should load the current state of your farm before enabling the user to change it. You can load the current state by doing a GET request for `https://hello-farm-ceb1d.firebaseio.com/farms/<your-farm-name>.json`.
 
